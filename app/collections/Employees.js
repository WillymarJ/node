'use strict';

const Employee = require('../models/Employee');

module.exports = bookshelf.Collection.extend({
    model: Employee,
});