'use strict';
const passport = require('passport');
const bcrypt = require('bcryptjs');
const http = require('http');

module.exports = {
    index: Async.route(function *(req, res, next) {
        // if (req.isAuthenticated()) {
        //     res.redirect('/dashboard');
        // }else {
        //     res.render('auth/login');
        // }
        res.redirect('/');
    }),

    login: Async.route(function *(req, res, next) {
        passport.authenticate('local',
            function(err, user, info) {
                if (err) return res.ok(err.message);

                if (!user) {
                    return res.ok(info);
                }

                return req.login(user, function(err) {
                    if (err) return res.ok(err.message);

                    return res.ok('1');
                });
            })(req, res);
    }),

    forgot: Async.route(function *(req, res, next) {
        var model = res.model;
        model.email = req.params.email;
        res.render('forgot', model);
    }),

    forgotProcess: Async.route(function *(req, res, next) {
        var employee = yield Employee.forge({email: req.body.email}).fetch().catch(console.error);
        if (_.isEmpty(employee)) return res.ok('The email you entered does not belong to any account.', http.STATUS_CODES[417], 417);

        var newPassword = Utils.uidUpperCase(5);
        var passHash = bcrypt.hashSync(newPassword, 8);
        var dataEmployee = {
            password: passHash,
        };

        const updated = yield employee.save(dataEmployee).catch(console.error);
        employee = employee.toJSON();
        var data = {
            nama: employee.name,
            email: employee.email,
            password: newPassword,
        };
        Email.send('emailForgotPassword', data, function(info) {
            return res.ok('Your password has been sent to ' + employee.email + ', please check your email.');
        });

    }),

    logout: Async. route(function*(req, res, next) {
        var employeeDb = yield Employee.forge({id: req.user.id}).fetch().catch(console.error);
        const updated = yield employeeDb.save({isLogin: false}).catch(console.error);
        req.logout();
        res.redirect('/');
    }),

};
