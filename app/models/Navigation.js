var navigation = {
    admin: [
        {
            url: '/dashboard',
            displayName: 'Dashboard',
            child: false,
            heading: false,
            icon: 'si-speedometer',
        },
        {
            url: '#',
            displayName: 'Apps Menu',
            child: false,
            heading: true,
            icon: '',
        },
        {
            url: '/employees',
            displayName: 'Manage Employee',
            child: false,
            heading: false,
            icon: 'si-user-following',
        },
        {
            url: '',
            displayName: 'Parent',
            child: [{
                url: '/child1',
                displayName: 'child 1',
                child: false,
                icon: 'si-docs',
            },
            {
                url: '/child2',
                displayName: 'child 2',
                child: false,
                icon: 'si-docs',
            }, ],
            icon: 'si-docs',
        },

    ],
};
global.Navigations = navigation;
