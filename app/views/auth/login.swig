<!DOCTYPE html>
<!--[if IE 9]>         <html class="ie9 no-focus"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-focus"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>Arsen CMS | Login</title>

        <meta name="description" content="OneUI - Admin Dashboard Template & UI Framework created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" type="image/png" href="/img/logo.png">

        <link rel="icon" type="image/png" href="/img/favicons/favicon-16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="/img/favicons/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="/img/favicons/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="/img/favicons/favicon-160x160.png" sizes="160x160">
        <link rel="icon" type="image/png" href="/img/favicons/favicon-192x192.png" sizes="192x192">

        <link rel="apple-touch-icon" sizes="57x57" href="/img/favicons/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/img/favicons/apple-touch-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/img/favicons/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/img/favicons/apple-touch-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/img/favicons/apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/img/favicons/apple-touch-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/img/favicons/apple-touch-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/img/favicons/apple-touch-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/img/favicons/apple-touch-icon-180x180.png">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Web fonts -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">

        <!-- Bootstrap and OneUI CSS framework -->
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" id="css-main" href="/css/oneui.css">

        <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
        <!-- <link rel="stylesheet" id="css-theme" href="/css/themes/flat.min.css"> -->
        <!-- END Stylesheets -->
        <style type="text/css">
        .pulldown{
            top: 50px !important;
        }
        </style>
    </head>
    <body>
        <!-- Login Content -->
        <div class="bg-white pulldown">
            <div class="content content-boxed overflow-hidden">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
                        <div class="push-50 animated fadeIn">
                            <!-- Login Title -->
                            <div class="text-center">
                                {# <h1>ARSEN</h1> #}
                                <img src="/img/logo-full.png" width="60%" class="img push-30">
                            </div>
                            <!-- END Login Title -->

                            <!-- Login Form -->
                            <!-- jQuery Validation (.js-validation-login class is initialized in js/pages/base_pages_login.js) -->
                            <!-- For more examples you can check out https://github.com/jzaefferer/jquery-validation -->
                            <form class="js-validation-login form-horizontal push-50-t form-login" action="/login" method="post">
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="form-material form-material-success floating">
                                            <input class="form-control" type="email" id="email" name="email" required>
                                            <label for="email">Email</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="form-material form-material-success floating">
                                            <input class="form-control" type="password" id="password" name="password" required>
                                            <label for="password">Password</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="font-s13 text-right push-5-t">
                                            {# <a href="">Forgot Password?</a> #}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group push-30-t">
                                    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                                        <button class="btn btn-sm btn-block btn-success" type="submit">Log in</button>
                                    </div>
                                </div>
                            </form>
                            <!-- END Login Form -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Login Content -->

        <!-- Login Footer -->
        <div class="pulldown push-30-t text-center animated fadeInUp">
            <small class="text-muted"><span class="js-year-copy"></span> &copy; NRA CMS</small>
        </div>
        <!-- END Login Footer -->

        <!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
        <script src="/js/core/jquery.min.js"></script>
        <script src="/js/core/bootstrap.min.js"></script>
        <script src="/js/core/jquery.slimscroll.min.js"></script>
        <script src="/js/core/jquery.scrollLock.min.js"></script>
        <script src="/js/core/jquery.appear.min.js"></script>
        <script src="/js/core/jquery.countTo.min.js"></script>
        <script src="/js/core/jquery.placeholder.min.js"></script>
        <script src="/js/core/js.cookie.min.js"></script>
        <script src="/js/app.js"></script>

        <!-- Page JS Plugins -->
        <script src="/js/plugins/jquery-validation/jquery.validate.min.js"></script>

        <!-- Page JS Code -->
        <script src="/js/pages/login.js"></script>


        <div class="modal fade " id="modal-notif" tabindex="-1" role="dialog" data-backdrop="static"  aria-hidden="true">
            <div class="modal-dialog modal-dialog-popout modal-sm">
                <div class="modal-content">
                    <div class="block block-themed block-transparent remove-margin-b">
                        <div class="block-header bg-danger">
                            <ul class="block-options">
                                <li>
                                    <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                                </li>
                            </ul>
                            <h3 class="block-title">Warning</h3>
                        </div>
                        <div class="block-content">
                            <p>The email you entered does not belong to any account.</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-sm" type="button" data-dismiss="modal">Ok</button>
                    </div>
                </div>
            </div>
        </div>


    </body>
</html>